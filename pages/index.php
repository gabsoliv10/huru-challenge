<?php require_once('../components/header.php'); ?>

<main class="page__home" role="main">
	
	<section class="form">
		<div class="container">
			<div class="row">
				<form>
					<div class="col-md-3 col-sm-12">
						<label>Status</label>
						<select id="status">
							<option value=""></option>
							<option value="OPEN">OPEN</option>
							<option value="CLOSED">CLOSED</option>
							<option value="AVAILABLE">AVAILABLE</option>
							<option value="SENT">SENT</option>
							<option value="RECEIVED">RECEIVED</option>
							<option value="APPLIED">APPLIED</option>
						</select>
					</div>
					
					<div class="col-md-3 col-sm-12">
						<label>Code</label>
						<input type="text" name="code" value="" id="code">
					</div>

					<div class="col-md-3 col-sm-12">
						<label>Type</label>
						<select id="type">
							<option value=""></option>
							<option value="box">BOX</option>
							<option value="meter_box">METER_BOX</option>
							<option value="bag">BAG</option>
							<option value="seal">SEAL</option>
							<option value="meter">METER</option>
							<option value="meter_pallet">METER_PALLET</option>
							<option value="meter_box">METER_BOX</option>
						</select>
					</div>

					<div class="col-md-3 col-sm-12">
						<label>Owner Role</label>
						<select id="owner">
							<option value=""></option>
							<option value="user">USER</option>
							<option value="admin">ADMIN</option>
							<option value="transport">TRANSPORT</option>
							<option value="warehouse">WAREHOUSE</option>
							<option value="lab">LAB</option>
						</select>
					</div>
					
				</form>
				<div class="col-lg-12">
					<button id="btn" class="btn btn-primary">Pesquisar</button>
				</div>
			</div>
		</div>
		
	</section>

	<section class="resultList">
		<div class="container">
			<div class="row">
				
				<div class="resultList__item"></div>

				<div class="col-md-12">
					<div class="resultList__item--no"></div>
				</div>
			</div>
		</div>
	</section>


	<div id="modal" class="modal fade" role="dialog">
		<div class="modal-dialog"> 
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button> 
					<h4 class="modal-title">Details - <span id="modalTitle" ></span></h4> 
				</div>
				<div id="modalBody" class="modal-body">
					<p>Loading...</p>
				</div> 
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
				</div>
			</div>
		</div>
	</div>


	<script>
		var token = "";			
		$("#btn").click(function(){
			$.ajax({
				url: 'https://dev.api.hurusystems.com/auth',
				dataType:"json",
				beforeSend: function (xhr){ 
					xhr.setRequestHeader('Authorization', "Basic " + btoa("admin@admin.com:huru1234")); 
				}
			})
			.done(function(data) {
				token = (data.token);
				loadingFilterList(token);
			})
		});
		
		function loadingFilterList(token) {
			var status = $('#status').val();
			var code = $("#code").val();
			var type = $('#type').val();
			var owner = $('#owner').val();

			var params = {};
			
			if(status) { params['status[in]'] = status;}
			if(code) { params['code'] = code;}
			if(type) { params['type[in]'] = code;}
			if(owner) { params['owner_role[in]'] = code;}

			console.log(status)

    		$.ajax({
			url: "https://dev.api.hurusystems.com/items/all",
			headers: {
			Authorization: "Basic " + token,
			token: token
			},
			beforeSend: function (xhr) {
				xhr.setRequestHeader ("Authorization", "Basic " + token);
			},
			method: "POST",
			dataType: "json",
			data: params,
		})
		.done(function(data){
			console.log("data", data)
			var card = "";

			for(let i=0; i < data.results.length; i++){
				card += '<div class="col-md-3 col-sm-12" data-id="'+data.results[i].code+'" onclick="showDetails(this)"> <div class="resultList__box">' +
				"<p><strong>Status</strong></p>" +
				"<span>"+ data.results[i].status +"</span>"+
				"<p><strong>Code</strong></p>" +
				"<span>"+ data.results[i].code +"</span>"+
				"<p><strong>Type</strong></p>" +
				"<span>"+ data.results[i].type +"</span>"+
				"<p><strong>Owner</strong></p>" +
				"<span>"+ data.results[i].owner_role +"</span>"+
				'<button type="button" class="btn btn-primary btn-modal" data-toggle="modal" data-target="#modal">' +
  				"More Details" +
				"</button>"+
				"</div> </div>";
			 }

			 if(!data.results.length) {
				var noResults = "<div>" +
				"<p><strong>Nenhum resultado encontrado!</strong></p>" +
				"</div>";
			}else {
				$('.resultList__item--no').remove()
			}

			$('.resultList__item').html(card);
			$('.resultList__item--no').html(noResults);
			 
		})
		.fail(function(){
			console.log("error", token) 
		})
		}

		function showDetails(e) {
			var code = $(e).data("id");

			$.ajax({
			url: "https://dev.api.hurusystems.com/inventory/"+code,
			headers: {
			Authorization: "Basic " + token,
			token: token
			},
			beforeSend: function (xhr) {
				xhr.setRequestHeader ("Authorization", "Basic " + token);
			},
			method: "GET",
			dataType: "json",
		})
		.done(function(data){
			console.log(data);
			obj = JSON.stringify(data)
			obj2 = obj.replace(/_/g, ' ').replace(/[.*+?^${}()|[\]\\""]/g, '').replace(/,/g, '<br>')

			$('#modalTitle').html(data.code);
			$('#modalBody').html(obj2);
		})
		}
</script>

</main>
