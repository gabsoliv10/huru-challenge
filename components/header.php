<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('head.php'); ?>
</head>
<body>

  <header class="main__header">
    <div class="container">
      <figure class="header__logo">
        <a href="" title="">
          <img class="logo__white img-responsive center-block" src="../assets/images/logo.png" title="Página Inicial" alt="Página Inicial">
        </a>
      </figure>
   </div>
 </header>